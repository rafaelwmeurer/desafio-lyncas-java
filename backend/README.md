## Backend da aplicação

### Tecnlogias utilizadas

- yarn
- nodejs 
- javascript
- mongodb
- docker

### Passo 1

Antes de subir o backend é importante que tenha feito a configuração do database, mongodb de forma local, seja utilizando o docker-compose.yml disponível na basta 'database' deste repositório, ou instalando o mongo no sistema operação.

Altere os arquivos

- config/default.json
- config/default.json

Se possuir alguma variável de ambiente do node, como NODE_ENV setada para ambientes development ou production, é importante que crie arquivos production.json ou develpoment.json, caso não seja feito, será utilizado o default.json para execução. O test.json é necessários para execução dos teste unitários.

Exemplo:

```
{
    "mongodb": {
        "url":"mongodb://localhost:27017/desafio-lyncas"
    },
    "porta": 3333
}
```

Outra opção é utilizar o mongodb atlas, basta ter a url que colocar no parâmetro no arquivo de config, assim como é utilizado na demo deste projeto.

----

### Passo 2

Como o banco de dados configurado e em execução, agora basta subir a aplicação, seja localmente ou então apenas executando o docker-compose para execução da aplicação


```
docker-compose up
```




