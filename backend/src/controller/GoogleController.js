const books = require('google-books-search');

module.exports = {
    async findAll (req, res) {
        books.search(req.params.q, function (error, results) {
            if (!error) {
                return res.send(results)
            } else {
                return res.send(error)
            }
        });
    },
}