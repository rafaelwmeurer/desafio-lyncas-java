
var Livro = require("../models/Livro");
//const db = require('../database/db');

module.exports = {
  async statusApi(_, res) {
    return res.status(200).send("Bem vindo a api rest...");
  },

  async listarTodos(_, res) {
    //db.connect();
    try {
      const livros = await Livro.find().sort("-createAt");
      return res.json(livros);
    } catch (e) {
      return res.json({ Erro: e });
    } /*finally {
            db.disconnect();
        }*/
  },

  async buscarLivroPorId(req, res) {
    //db.connect();
    try {
      const livro = await Livro.findOne({ _id: req.params._id });
      if (livro === null) {
        return res.json({ result: "Registro não existe." });
      } else {
        return res.json(livro);
      }
    } catch (e) {
      return res.json({ Erro: e });
    } /*finally {
            db.disconnect();
        }*/
  },

  async novoLivro(req, res) {
    //db.connect();
    try {
      const { authors, title, description, link, image } = req.body;
      const livro = await Livro.create({
        title,
        authors,
        description,
        link,
        image
      });
      return res.json(livro);
    } catch (e) {
      return res.json({ Erro: e });
    } /*finally {
            db.disconnect();
        }*/
  },

  async atualizaLivro(req, res) {
    //db.connect();
    try {
      await Livro.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true },
        (err, Livro) => {
          if (err) {
            res.send({ Erro: err });
          }
          if (Livro === null) {
            return res.json({
              result:
                "Nenhum registro encontrado com o id, portanto nada foi atualizado."
            });
          } else {
            res.json(Livro);
          }
        }
      );
    } catch (e) {
      res.json({ Erro: e });
    } /*finally {
            db.disconnect();
        }*/
  },

  async deletarLivro(req, res) {
    //db.connect();
    try {
      const livro = await Livro.deleteOne({ _id: req.params._id });
      if (livro.n === 0) {
        return res.json({
          result:
            "Nenhum registro encontrado com o id, portanto nada foi eliminado."
        });
      } else if (livro.n === 1) {
        return res.json({ result: "Registro eliminado com sucesso." });
      }
    } catch (e) {
      return res.json({ Erro: e });
    } /*finally {
            db.disconnect();
        }*/
  }
};
