const mongoose = require("mongoose");
const config = require("config");

module.exports = {
  mongoose,
  connect: () => {
    mongoose.Promise = Promise;
    mongoose.connect(config.get("mongodb.url"), {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  },
  disconnect: done => {
    mongoose.disconnect(done);
  }
};
