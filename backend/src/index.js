const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("config");

const app = express();

const server = require("http").Server(app);
const io = require("socket.io")(server);

mongoose.connect(config.get("mongodb.url"), {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

app.use((req, res, next) => {
  req.io = io;
  next();
});

app.use(bodyParser.json());
app.use(cors());

app.use(require("./routes"));

server.listen(config.get("porta"));
//console.log(`Listening in port ${config.get('porta')}`);

module.exports = { app, server };
