const mongoose = require("mongoose");

const LivroSchema = new mongoose.Schema(
  {
    title: String,
    authors: Array,
    description: String,
    link: String,
    image: String
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Livro", LivroSchema);
