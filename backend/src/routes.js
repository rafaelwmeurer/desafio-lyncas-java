const express = require("express");
const LivroController = require("./controller/LivroConroller");
const ApiGoogle = require("./services/ApiGoogle");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const jsonParser = bodyParser.json();

const routes = new express.Router();

routes.get("/", LivroController.statusApi);
routes.get("/favoritos", LivroController.listarTodos);
routes.get("/favoritos/:_id", LivroController.buscarLivroPorId);
routes.delete("/favoritos/:_id", LivroController.deletarLivro);
routes.post("/favoritos", jsonParser, LivroController.novoLivro);
routes.put("/favoritos/:id", jsonParser, LivroController.atualizaLivro);

routes.get("/google/", ApiGoogle.findAllUndefined);
routes.get("/google/:q", ApiGoogle.findAll);

routes.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

module.exports = routes;
