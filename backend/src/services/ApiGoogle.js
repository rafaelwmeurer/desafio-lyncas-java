const books = require("google-books-search");

module.exports = {
  async findAll(req, res) {
    try {
      books.search(req.params.q, function(error, results) {
        if (!error) {
          return res.send(results);
        } else {
          return res.send(error);
        }
      });
    } catch (e) {
      return res.json({ Erro: e });
    }
  },
  async findAllUndefined(req, res) {
    try {
      books.search("undefined", function(error, results) {
        if (!error) {
          return res.send(results);
        } else {
          return res.send(error);
        }
      });
    } catch (e) {
      return res.json({ Erro: e });
    }
  }
};
