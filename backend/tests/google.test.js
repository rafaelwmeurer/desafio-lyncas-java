const request = require('supertest');
const { app, server } = require("../src/index");

describe('Testando rotas que fazem consumo da api do google', () => {
    beforeAll(async () => {
        server.close();
    });

    it("GET / route, verificando se o serviço da api do google está responde", async () => {
        const query = "teste";
        return request(app)
            .get(`/google/${query}`)
            .expect(200)
            .then(res => {
                expect(res.body).toMatchSnapshot();
            });
    });
})