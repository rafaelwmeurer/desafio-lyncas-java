const request = require('supertest');
const { app, server } = require("../src/index");

describe('Testando rotas de dados dos livros favoritos', () => {
    beforeAll(async () => {
        server.close();
    });

    it("GET / route, verificando se o serivço responde", async () => {
        return request(app)
            .get("/")
            .expect(200)
            .then(res => {
                expect(res.body).toMatchSnapshot();
            });
    });

    it("GET /favoritos route , listando todos os livros favoritos", async () => {
        return request(app)
            .get("/favoritos")
            .expect(200)
            .then(res => {
                expect(res.body).toMatchSnapshot();
            });
    });

    it("POST /favoritos , salvar novo livro como favorito", async () => {
        const favorito = {
            authors: ["test"],
            title: "teste 22222",
            description: "um bom livro",
            link: "http://teste.com",
            image: "http://teste.com/teste.png"
        };

        return request(app)
            .post("/favoritos")
            .send(favorito)
            .expect(200)
            .then(res => {
                expect(res.body).toMatchSnapshot();
                console.log(res.body._id);
            });
    });
})