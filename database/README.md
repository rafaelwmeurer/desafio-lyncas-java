## Database da aplicação

Para utilizar o mongodb localmente utilizando docker com as configurações do docker-compose da pasta 'database', basta executar o comando:

Mude a senha dentro do arquivo caso assim preferir, pois o que for colocado aqui precisará ser ajustado no config do backend.

```
docker-compose up 
```

Ou 

```
docker-compose up -d
```

Indico usar a segunda opção, pois poderá fechar a janela onde foi executado o comando sem problemas.

Se preferir utilizar estas configurações gerando uma imagem para assim iniciar um container, também funciona, basta pegar essas configurações e criar o Dockerfile.